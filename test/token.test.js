const { expect } = require('chai')
const { ethers } = require('hardhat')

let accounts

async function deployToken () {
  accounts = await ethers.getSigners()

  const Token = await ethers.getContractFactory('RubusFundBlackToken')
  const token = await Token.deploy()
  await token.deployed()

  return token
}

describe('token', () => {
  describe('Balance logic', () => {
    let coinInstance

    beforeEach(async () => {
      coinInstance = await deployToken()
    })

    it('should put 1mln token in the first account', async () => {
      const balance = await coinInstance.balanceOf(accounts[0].address)

      expect(balance.valueOf()).to.equal('1000000000000000000000000000')
    })

    it('should send coin correctly', async () => {
      // Setup 2 accounts.
      const accountOne = accounts[0].address
      const accountTwo = accounts[1].address

      // Get initial balances of first and second account.
      const accountOneStartingBalance = (await coinInstance.balanceOf(accountOne))
      const accountTwoStartingBalance = (await coinInstance.balanceOf(accountTwo))

      // Make transaction from first account to second.
      const amount = 10
      await coinInstance.transfer(accountTwo, amount, { from: accountOne })

      // Get balances of first and second account after the transactions.
      const accountOneEndingBalance = (await coinInstance.balanceOf(accountOne))
      const accountTwoEndingBalance = (await coinInstance.balanceOf(accountTwo))

      expect(accountOneEndingBalance).to.equal(accountOneStartingBalance.sub(amount))
      expect(accountTwoEndingBalance).to.equal(accountTwoStartingBalance.add(amount))
    })
  })
})
